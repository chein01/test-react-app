import React, { Component } from 'react';
import './App.css';
import TodoItem from './components/TodoItem.js';
import TrafficLight from './components/TrafficLight.js';
import tick from './img/list.svg';

//Class TodoItem 
class App extends Component {
  constructor() {
    super();
    this.state = {
      newItem: '',
      todoItems: [
        { 'title': 'ABC', isComplete: true },
        { 'title': 'DEF' },
        { 'title': 'GHI' }
      ]
    }
    this.onKeyUp = this.onKeyUp.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  onItemClicked(item) {
    return (event) => {
      const isComplete = item.isComplete;
      const { todoItems } = this.state;
      const index = todoItems.indexOf(item);
      console.log(index);
      this.setState({
        todoItems: [
          ...todoItems.slice(0, index),
          { ...item, isComplete: !isComplete },
          ...todoItems.slice(index + 1)
        ]
      })
      console.log(...todoItems.slice(index))
    }
  };
  onKeyUp(event) {
    let text = event.target.value;
    if (event.keyCode === 13) {
      if (!text) {
        return;
      }
      text = text.trim();
      if (!text) { return; }
      this.setState({
        newItem: '',
        todoItems: [
          { 'title': text, 'isComplete': true },
          ...this.state.todoItems
        ]
      })
    }
  }

  onChange(event){
    this.setState({
      newItem: event.target.value
    })
  }
  render() {
    const { todoItems, newItem } = this.state;
    if (todoItems.length) {
      return (
        <div className="App">
          <div className="Header">
            <img src={tick} width={32} height={32} />
            <input 
            type="text" 
            value = {newItem}
            onChange = {this.onChange}
            placeholder="Add a new item" 
            onKeyUp={this.onKeyUp} />
          </div>
          {todoItems.length && todoItems.map((item, index) => (
            <TodoItem
              key={index}
              item={item}
              onClick={this.onItemClicked(item)} />
          ))}
        </div>
      );
    } else {
      return (
        <div className="App">Nothing here</div>
      );
    }
  }
}

/*
// Class Traffic Light
class App extends Component {
  render(){
    return (
      <div className="App">
        <TrafficLight/>
      </div>
    );
  }
}

*/
export default App;



